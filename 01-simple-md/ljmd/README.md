# ljmd.cpp
A `C++` implementation of simple molecular dynamics (MD) simulations with the Lennard-Jones (LJ) potential. 

## Compile

```shell
g++ -std=c++11 -O2 ljmd.cpp -o ljmd.exe # using g++ from GCC
cl.exe /O2 ljmd.cpp -o ljmd.exe         # using cl.exe from MSVC
```

## Run

```shell
./ljmd.exe < ex.in   # Linux
ljmd.exe < ex.in     # Windows
```
